<?php
require_once 'require.php';
//加载配置
$config = include_once 'config.php';
define('BASE_PATH', str_replace('\\', '/', __DIR__."/"));
//检查攻击
$checkAttack = new CheckAttack();
if (!$checkAttack->check()) {
   echo  html($config['attackTitle']);
    die;
}
//过滤关键词
$PpRubish   = new PpRubish(BASE_PATH.$config['ppRubish']);
$rubish     = $PpRubish->getData();
//加载页面数据
$page = new Page([
    'keyword'   => @$_GET['s'],
    'ppRubish'  => $rubish['pp'],
    'rpRubish'  => $rubish['rp'],
    'config'    => $config
]);

//渲染静态页面
require 'Render.php';

?>

