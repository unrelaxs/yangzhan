<?php
return [
    'dir'                   => '/', //自定义文件所在目录，例如想把文件放在根目录下 a 目录，'/' 改为 '/a/'
    'baiduImgShowType'      => '1', //想展现百度图片请把 0 改为 1，保存百度图片请改为 2(不推荐使用)
    'Https'                 => false,
    'urlType'               => '0', //想用绝对地址请把 0 改为 1
    'linkType'              => '0', // 改为 1 启用伪静态(不推荐使用)

    'cacheDir'              => 'stock/', //缓存目录
    'cacheClear'            => 0,       // 当缓存过多影响服务器，可以将 0 改为 1 并保存，约4分钟后即可自动清空所有缓存，最后在原有位置恢复 0
    'indexKeywordCache'     => 14400,   //每隔2小时更新一次首页关键词列表
    'indexUpdateTab'        => 'public/cache/cwhtr', // 首页更新记号


    'searchPlatform'        => [
        'baiduHot'      => 'public/cache/yizhiwangnanfangkai',    // 百度搜索实时热点
        'wangyiMusic'   => 'public/cache/ruguohaiyoumingtian',   //网易音乐
        'sogouHot'      => 'public/cache/haikuotiankong',        // 搜狗热搜榜
        'doubianFilm'   => 'public/cache/the+mass'               // 豆瓣最新电影
    ],

    'titlePrefix'           => '聚合搜索',  // 自定义标题后缀
    'titleLimit'            => 48,          // 自定义标题字数上限(48 相当于 24 个汉字长度)
    'description'           => '还你一个没有百度推广、产品的搜索结果页',   // 默认元描述
    'httpType'              => 0,           // 如果是 https 网站 请把 0 改为 1
    'footerLink'            => '0', // 想用页脚友情链接请把 0 改为 1
    'friendLink'            => [            // 可在数组里添加或删除友链
        ['http://www.apple.com/cn/', '苹果'],
        ['https://www.tmall.com/', '天猫'],
    ],

    'ppRubish'      => 'ppRubish.txt',   //反垃圾
    'attackTitle'   => '已过滤跨站脚本攻击漏洞, 到别处看看罢',   //当夸站攻击时，提示

    'proxyToken'    => '',// 使用自动更新代理 IP 功能请先关注 天香空城 微信号 ulisse 免费申请注册码，然后将注册码填进单引号内保存
    'proxyIp'       => [    // 手动添加代理 IP，用于反百度自动屏蔽机制(建议至少添加 1 个中国内地 IP)

    ],
    'require'   => [
        'np'    => 'config/np.php',
        'nr'    => 'config/nr.php',
        'srcid' => 'config/srcid.php'
    ]
];