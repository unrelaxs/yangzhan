<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" lang="cmn-Hans" xml:lang="cmn-Hans">
<head>
    <meta charset="UTF-8">
    <title><?php echo $page->Title() ?></title>
<meta content="<?php echo $page->Description() ?>" name="description">
<meta content="<?php echo $page->Keyword() ?>" name="keywords">
<meta content="webkit" name="renderer">
<meta content="telephone=no" name="format-detection">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black" name="apple-mobile-web-app-status-bar-style">
<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0,minimal-ui" name="viewport">
<link href="<?php echo $page->Canonical() ?>" rel="canonical">
<link rel="stylesheet" type="text/css" href="public/init.css">
</head>
<body itemscope itemtype="http://schema.org/WebPage">
    <div class="detail">
        <div class="header center non">
            <?php echo $page->HeaderSearchForm() ?>
        </div>
        <?php $keyword = $page->keyword; if (empty($keyword)) :  ?>
        <div class="back-yellow" style="padding: 1em;">
            <?php echo $page->TogetherSearch() ?>
            <p>
                <?php echo $page->FriendLink() ?>
            </p>
        </div>
        <?php endif; ?>
        <!--搜索搜索-->
        <?php echo $page->KeyWordSearchCount()?>
        <!--搜索列表-->
        <!--分页-->
        <div class="center back-white">
            <?php echo $page->Paginate()?>
        </div>
        <!--分页-->
    </div>
</body>
</html>
<script src="public/baidu.js"></script>
<script>
    (function(){
        var bp = document.createElement('script');
        var curProtocol = window.location.protocol.split(':')[0];
        if (curProtocol === 'https') {
            bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
        }
        else {
            bp.src = 'http://push.zhanzhang.baidu.com/push.js';
        }
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(bp, s);
    })();
</script>
