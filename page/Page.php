<?php
 class Page {
     protected $keyword;    //搜索关键字
     protected $GET;        //$_GET请求数据
     protected $POST;       //$_POST请求数据
     protected $sug1;       //百度搜索下拉框模式
     protected $config;     //配置

     protected $cache;      //缓存
     protected $search;     //查询

     protected $html = [];       //html对象

     protected $urlTarget;

    public function __construct($data)
    {
        $this->GET      = $_GET;
        $this->POST     = $_POST;
        $this->keyword  = $data['keyword'];
        $this->ppRubish = $data['ppRubish'];
        $this->rpRubish = $data['rpRubish'];
        $this->config   = $data['config'];

        $this->initConfig();
        $this->cache    = new FCache();
        $this->urlTarget= new Url($this);

        $this->initSearch();


    }
     public function __get($name)
     {
         if(!property_exists($this, $name))
         {
             exit(__DIR__.':不存在属性:'.$name);
         }
         return $this->$name;
     }

     public function __call($name, $arguments)
     {
        if (array_key_exists($name, $this->html))
        {
            return $this->html[$name]->get();
        } else {
            if (class_exists($name))
            {
                $temp = new $name($this);
                if ($temp instanceof Html)
                {
                    $this->html[$name] = $temp;
                    return $temp->get();
                } else {
                    return false;
                }
            }
            return false;
        }
     }

     public function initConfig()
     {
         if (empty($this->config['proxyToken']))
         {
             $this->config['proxyIp'] = [];
         }
     }

     public function setHtml($name)
     {
         echo date('H:i:s');
         echo '<br>';
         $html = include_once 'HtmlNew.php';
         foreach ($html as $value)
         {
             if (class_exists($value))
             {
                 $temp = new $value($this);
//                 if ($temp instanceof Html)
//                 {
//                     $this->html[$value] = $temp;
//                 }
             }
         }
         echo date('H:i:s');
         die;
     }

     public function initSearch()
     {
         $this->search = new Search($this->keyword, $this->ppRubish, $this->rpRubish);
         $this->search->search();
     }

     public function initData()
     {
      
     }

 }