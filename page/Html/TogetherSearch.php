<?php
//聚合搜索
class TogetherSearch implements Html{
    protected $page;
    protected $searchAble;
    protected $searchCacheFile;
    public function __construct(Page $page)
    {
        $this->page = $page;
        $this->setSearch();
    }

    public function get()
    {

        $keyword = $this->page->keyword;
        //只有无搜索关键词 才显示
        if (!empty($keyword))
        {
            return '';
        }
        $table       = $this->getTableData($this->getCacheData());
        $title = <<<html
        <h1 class="center bold" itemprop="name">{$this->page->config['titlePrefix']}</h1>
            <hr>
html;
        return $title.$table;
    }
    public function getPage()
    {
        return $this->page;
    }

    public function setSearch()
    {

        if (!empty($this->searchAble))
        {
            return;
        }
        $cacheFile = [];

        $searchArr = [
            new BaiduSearch($this),
            new DoubanSearch($this),
            new SogoSearch($this),
            new WangyiSearch($this)
        ];
        foreach($searchArr as $temp)
        {
            if ($temp instanceof SearchInterface)
            {
                //判断是否更新缓存
                if ($this->isResh())
                {
                    $temp->search();
                    $this->updateRhc();
                }
                $this->searchAble[]             = $temp;
                $this->searchCacheFile[]        = $temp->getCacheFile();
            }
        }

    }

    public function getTableData($data)
    {
        foreach ($data as $i => $v)
        {
            if (empty(trim($v)))
            {
                unset($data[$i]);
            }
        }
        $data = array_values($data);
        if (strlen($data[0]) > 0) {
            $table = <<<html
                <table>
                    <tbody class="back-yellow">
html;
            shuffle($data);
            foreach ($data as $i => $v) {
                //每三个换一行
                if ($i % 3 == 0) {
                    $table.='<tr>';
                }
                $table.='<td><a itemprop="url" href="'.$this->page->urlTarget->getUrl().$this->page->urlTarget->link.preg_replace('/(\s+)/', '%20', $v).'" target="_blank">'.$v.'</a></td>';
                $i++;
                if ($i % 3 == 0) {
                    $table.='</tr>';
                }
                if ($i > 38) {
                    break;
                }
            }
//            if ($i % 3 == 1 || $i % 3 == 2)
//            {
//                $table .= '<td>百度参数分析</td></tr>';
//            }
            $table .= '</tbody></table>';
        }

        return $table;
    }

    public function updateRhc()
    {
        $rhc = $this->page->config['indexUpdateTab'];
        file_put_contents($rhc, '', LOCK_EX);
    }

    //判断是否更新缓存
    public function isResh()
    {
        //缓存时间到期，则刷新数据
        $rhc = $this->page->config['indexUpdateTab'];

        return !file_exists($rhc)
                    || (time() - filemtime($rhc) + 1) < $this->page->config['indexKeywordCache'];
    }

    public function getCacheData()
    {
        $word = "";
        if (!empty($this->searchCacheFile))
        {
            foreach ($this->searchCacheFile as $file)
            {
                if (file_exists($file))
                {
                    $word.= file_get_contents($file);
                }
            }
            $word = explode("\n", $word);
        }
        return $word;
    }

}