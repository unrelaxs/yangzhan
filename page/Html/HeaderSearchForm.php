<?php
class HeaderSearchForm implements Html
{
    protected $page;
    protected $inputs;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function get()
    {
        $page   = $this->page;
        $url    = $page->urlTarget->getUrl();
        $keyword= htmlspecialchars(@$page->keyword, ENT_QUOTES);
        $get    = $page->GET;
        $rn     = htmlspecialchars(@$get['rn'], ENT_QUOTES);
        $inputs = '';
        $html = <<<html
    <form method="get" action="{$url}">
            <a itemprop="url" href="{$url}" rel="nofollow">
                <canvas id="myCanvas" width="52" height="26">no canvas</canvas>
            </a>
            <script type="text/javascript">
                var canvas=document.getElementById("myCanvas");
                if(canvas.getContext){
                    var ctx=canvas.getContext("2d");
                    ctx.font="22px Helvetica Neue";
                    ctx.fillStyle="#FF6347";
                    ctx.fillText("百度",0,24);
                };
            </script>
            <input class="text" type="text" value="{$keyword}" name="s" autocomplete="off" maxlength="76" id="sug" autofocus="autofocus" placeholder="请输入查询词" onmouseover="this.focus()" onfocus="this.select()">
            <input class="submit" type="submit" value="百度一下"><br>
            <span class="white">
            </span>
    </form>
html;
        return $html;
    }


    public function getPage()
    {
        return $this->page;
    }
}