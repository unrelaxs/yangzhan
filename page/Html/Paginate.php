<?php
class Paginate implements Html {
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function get()
    {
        $keyword = $this->page->keyword;
        //只有无搜索关键词 才显示
        if (empty($keyword))
        {
            return '';
        }
        $url    = $this->page->urlTarget->getUrl();
        $query  = $this->page->search->query;
        $pn     = $this->page->search->pn;
            // 翻页
            $pni = array(750, 740, 730, 720, 50, 40, 30, 20, 10);
            $table ='    <br><table class="center back-white"><tbody><tr>';
            if ($pn + 1 > 10) {
                $table.= '
        <td><a href="'.$url.'?s='.$query.'&amp;pn='.(floor($pn / 10) * 10 - 10).'" rel="nofollow">上页</a></td>';
            }
            foreach ($pni as $k => $v) {
                if ($pn + 1 > $pni[$k] ) {
                    $table.= '
        <td><a href="'.$url.'?s='.$query.'&amp;pn='.(floor($k + ($pn / 10) - 8) * 10 - 10).'" rel="nofollow">'.floor(($k * 10 + $pn - 80) / 10) .'</a></td>';
                }
            }
            echo '
        <td class="pink">'.floor(($pn + 10) / 10).'</td>';
            foreach ($pni as $k => $v) {
                if ($pn < $pni[$k]) {
                    $table.= '
        <td><a href="'.$url.'?s='.$query.'&amp;pn='.(floor($k + ($pn / 10) + 2) * 10 - 10).'" rel="nofollow">'.floor(($k * 10 + $pn + 20) / 10) .'</a></td>';
                }
            }
            if ($pn < 750) {
                $table.= '
        <td><a href="'.$url.'?s='.$query.'&amp;pn='.(floor(($pn / 10) + 2) * 10 - 10).'" rel="nofollow">'.'下页</a></td>';
            }
        $table.= '
    </tr></tbody></table>
';
        return $table;
    }
    public function getPage()
    {
        return $this->page;
    }
}