<?php
class Description implements Html{
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }
    public function get()
    {
        $page   = $this->page;
        $desc   = '';
        $bk     = json_decode(file_get_contents('http://baike.baidu.com/api/openapi/BaikeLemmaCardApi?format=json&appid=379020&bk_key='.@$page->search->query), 1);
        $stk    = $page->config['cacheDir'];
        if (file_exists($stk.'abs-'.urlencode(@$page->search->query).'.txt')) {
            $desc = file_get_contents($stk.'abs-'.urlencode($page->search->query).'.txt');
        }
        elseif (strlen(@$bk['abstract']) > 0) {

            $desc = str_replace('...', '', str_replace($page->search->ppRubish, $page->search->rpRubish, $bk['abstract']));
            if (file_exists($stk.'abs-'.urlencode(@$page->search->query).'.txt') == false) {
                file_put_contents(
                    $stk.'abs-'.urlencode($page->search->query).'.txt',
                    str_replace('...', '', str_replace($page->search->ppRubish, $page->search->rpRubish, $bk['abstract']))
                    , LOCK_EX
                );
            }
        }

        return $desc;
    }

    public function getPage()
    {
        return $this->page;
    }
}