<?php
interface Html {
    public function get();
    public function getPage();
}