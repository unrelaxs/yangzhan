<?php
class Title implements Html{
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }
    public function get()
    {
        $page = $this->page;
        $querys = $page->search->querys;
        $title = '';
        $pt    = $page->config['titlePrefix'];
        $wpt    = wordcount($pt);
        $wz     = wordcount(htmlspecialchars($page->search->rpData, ENT_QUOTES));
        $wsug   = wordcount(@$page->search->sugl[1][0]);
        $wcache = wordcount($page->cache->get(urlencode($querys)));
        if (strlen($page->cache->get(urlencode($querys))) > 0 && ($wcache + $wz) < $page->config['titleLimit']) {
            $title = $page->cache->get(urlencode($querys)).'_';
        }
        else {
            // 百度搜索框下拉模式 1 第 1 位查询扩展作为主标题
            if (strlen(@$page->search->sugl[1][0]) > 0) {
                if (($wsug + $wz) < $page->config['titleLimit']) {
                    echo str_replace($page->search->ppRubish, $page->search->rpRubish, $page->search->sugl[1][0]).'_';
                }
                $page->cache->add(urlencode($querys), str_replace($page->search->ppRubish, $page->search->rpRubish, $page->search->sugl[1][0]));
            }
        }
        // 引号转换为 HTML 实体的查询词作为副标题
        echo htmlspecialchars($page->search->rpData, ENT_QUOTES);
        // 标题后缀，品牌名
        if ((($wz + $wpt) < ($page->config['titleLimit'] - 1) && ($wsug + $wz) > $page->config['titleLimit']) || ($wsug + $wz + $wpt) < ($page->config['titleLimit'] - 1)) {
            $title = $title. '_'.$pt;
        }
        elseif ($wcache > 0 && ((($wz + $wpt) < ($page->config['titleLimit'] - 1) && ($wcache + $wz) > $page->config['titleLimit']) || ($wcache + $wz + $wpt) < ($page->config['titleLimit'] - 1))) {
            $title = $title. '_'.$pt;
        }
        return $title;
    }

    public function getPage()
    {
        return $this->page;
    }
}