<?php
class FriendLink implements Html{
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }
    public function get()
    {

        $page = $this->page;
        $a = [];
        if ($page->config['footerLink'] == 1 && !empty($page->config['friendLink'])) {
            foreach ($page->config['friendLink'] as $v)
            {
                $a[] =  ' <a href="'.$v[0].'" target="_blank" itemprop="url">'.$v[1].'</a> ';
            }
        }
        return implode('', $a);
    }

    public function getPage()
    {
        return $this->page;
    }
}