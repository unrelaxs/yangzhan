<?php
class Keyword implements Html{
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }
    public function get()
    {
        $page   = $this->page;
        $pp     = $page->search->ppRubish;
        $rp     = $page->search->rpRubish;
        $querys = $page->search->querys;
        $sug1   = $page->search->sug1;
        $pt     = $page->config['titlePrefix'];
        $cache  = $page->cache;
        if (strlen($page->keyword) > 0) {
            if (strlen($cache->get(urlencode($querys))) > 0 && $cache->get(urlencode($querys)) != $pt) {
                echo str_replace($pp, $rp, $cache->get(urlencode($querys))).',';
            }
            elseif (strlen(@$sug1[1][0]) > 0 && @$sug1[1][0] != $pt) {
                echo str_replace($pp, $rp, $sug1[1][0]).',';
            }
            if (strlen(@$sug1[1][1]) > 0 && @$sug1[1][1] != $pt) {
                echo str_replace($pp, $rp, $sug1[1][1]).',';
            }
            if (strlen(@$sug1[1][2]) > 0 && @$sug1[1][2] != $pt) {
                echo str_replace($pp, $rp, $sug1[1][2]).',';
            }
            if (strlen(@$sug1[1][3]) > 0 && @$sug1[1][3] != $pt) {
                echo str_replace($pp, $rp, $sug1[1][3]).',';
            }
            if (strlen(@$sug1[1][4]) > 0 && @$sug1[1][4] != $pt) {
                echo str_replace($pp, $rp, $sug1[1][4]).',';
            }
            echo htmlspecialchars($page->search->rpData, ENT_QUOTES).',';
        }
    }

    public function getPage()
    {
        return $this->page;
    }
}