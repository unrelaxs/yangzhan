<?php
class Canonical implements Html{
        protected $page;
        protected $link;

        public function __construct(Page $page)
        {
            $this->page = $page;
        }
        public function get()
        {
        $urlTarget  = $this->page->urlTarget;
        $querys     = (string)@$this->search->querys;
        $this->link =
            $urlTarget->http.'://'.$_SERVER['HTTP_HOST'].$urlTarget->getUrl().
            $urlTarget->link.
            $querys
            ;
        return $this->link;
    }

    public function getPage()
    {
       return $this->page;
    }
}