<?php
//当有关键词时候，搜索
class KeyWordSearchCount implements Html{
    protected $page;
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function get()
    {
        $keyword = $this->page->keyword;
        //只有无搜索关键词 才显示
        if (empty($keyword))
        {
            return '';
        }
        $data   = $this->getBaiduSearch();

        return 
            '<div class="white break center">'
            .$this->getCountHtml($data)
            .$this->getNotFindHtml($data)
            .$this->getNotUrlHtml($data)
            .$this->getWordLimitHtml($data)
            .$this->getSizeHtml($data)
            .'</div>'
            .'<div class="draglist">'
            .$this->getProduct($data)
            .'</div>'
            ;
    }
    public function getPage()
    {
        return $this->page;
    }

    public function getBaiduApi()
    {

        $config         = [
            'url'   => 'http://www.baidu.com/s?wd=',
            'query' => $this->page->search->query,
            'pn'    => $this->page->search->pn,
            'cpn'   => '&pn='
        ];
        $baseKeyWord    = new SearchKeyWord($config);

        $pnBase         = new PnSearchKeyWord($baseKeyWord, $config);
        $pnRn           = new RnSearchKeyWord($pnBase, $config);
        $pnGpc          = new GpcSearchKeyWord($pnBase, $config);
        $pnRnGpc        = new GpcSearchKeyWord($pnRn, $config);

        $rnBase         = new RnSearchKeyWord($baseKeyWord, $config);
        $rnGpc          = new GpcSearchKeyWord($rnBase, $config);

        $gpcBase        = new GpcSearchKeyWord($baseKeyWord, $config);

        //按顺序依赖
        $keyword = [
//            $pnRnGpc,
//            $pnRn,
//            $pnGpc,
            $pnBase,
            $rnGpc,
            $rnBase,
            $gpcBase,
            $baseKeyWord
        ];

        //获取 搜索关键字的api地址
        $api = $baseKeyWord->getUrl();
        foreach ($keyword as $word)
        {
            if ($word->condition())
            {
                $api = $word->getUrl();
                break;
            }
        }
        return $api;
    }

    public function getBaiduSearch()
    {
        $api = $this->getBaiduApi();
        $se = $res = file_get_contents($api);

        if (empty($res)) {
            if  (!empty($this->page->config['proxyIp'])) {
                $proxy = $this->page->config['proxyIp'];
                $config = [];
                $baseKeyWord    = new SearchKeyWord($config);
                shuffle($proxy);
                $c = curl_init();
                curl_setopt($c, CURLOPT_HEADER, 0);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_TIMEOUT, 4);
                curl_setopt($c, CURLOPT_PROXY, $proxy[0]);
                curl_setopt($c, CURLOPT_URL, $baseKeyWord->getUrl());
                $se = curl_exec($c);
                curl_close($c);
            }
        }
        return $se;
    }

    public function getCountHtml($se)
    {
        $now    = date('Y-m-d H:i:s');
        $str = htmlspecialchars($this->page->keyword, ENT_QUOTES);
        if (preg_match('/百度为您找到相关结果约([0-9,]{1,11})/ius', @$se, $mnum));
        if (empty($mnum))
        {
            $mnum[1] = 2;
        }
        return <<<html
            <h1 style="display: inline;" itemprop="name" title="$mnum[1] 条结果">{$str}</h1>
            <br>
            <span title="日期">{$now}</span>
html;
    }

    public function getNotFindHtml($se)
    {
        if (preg_match('/(?<=<p>很抱歉，没有找到与<span style="font-family:宋体">“<\/span><em>)(.+)(?=<\/em><span style="font-family:宋体">”<\/span>相关的网页。<\/p>)/', @$se, $mno)) {
            return  '
                <p>
                    <a itemprop="url" class="noa" href="//'.$mno[1].'" target="_blank" rel="external nofollow noreferrer" title="直接访问&nbsp;'.@$mno[2].'">很抱歉，没有找到与“<span class="red">'.$mno[1].'</span>”相关的网页。</a>
                </p>
                <p class="white">如网页存在，请
                    <a itemprop="url" class="noa" href="//zhanzhang.baidu.com/linksubmit/url?sitename=http%3A%2F%2F'.$mno[1].'" target="_blank" rel="external nofollow noreferrer" title="您可以提交想被百度收录的url">提交网址</a>给我们
                </p>
';
        }
        return '';
    }

    public function getNotUrlHtml($se)
    {
        // 冇收录，但有其他搜索结果
        if (preg_match('/(?<=<font class="c-gray">没有找到该URL。您可以直接访问&nbsp;<\/font><a href=")(.+)(?=" target="_blank" onmousedown)/', @$se, $mno2)) {
            return  '
                <p class="white">没有找到该URL。您可以直接访问&nbsp;<span class="red"><a itemprop="url" class="noa" href="'.$mno2[1].'" target="_blank" rel="external nofollow noreferrer" title="直接访问 '.$mno2[1].'">'.$mno2[1].'</a></span>，还可<a itemprop="url" class="noa" href="//zhanzhang.baidu.com/sitesubmit/index?sitename='.$mno2[1].'" target="_blank" rel="external nofollow noreferrer" title="您可以提交想被百度收录的url">提交网址</a>给我们。</p>
                ';
        }
        return '';
    }

    public function getWordLimitHtml($se)
    {
        // 字数限制
        if (preg_match('/(?<=><strong>)(.+)(?=&nbsp;及其后面的字词均被忽略，因为百度的查询限制在38个汉字以内。<\/strong><)/', @$se, $mlimit)) {
            return '<p class="white">'.$mlimit[1].'&nbsp;及其后面的字词均被忽略，因为百度的查询限制在38个汉字以内。</p>';
        }
        return '';
    }

    public function getSizeHtml($se)
    {
        if (preg_match('/(([\w\-\x80-\xff]{1,63}\.)*[\w\-\x80-\xff]{1,63}\.(com|cn|club|red|loan|bid|wang|ren|xyz|news|video|top|net|site|online|website|space|biz|win|date|click|link|pics|cc|tech|xin|photo|party|trade|science|pub|rocks|band|market|help|gift|press|wiki|design|software|social|lawyer|engineer|live|studio|org|me|name|info|tv|mobi|asia|co|io|gov|edu|uk|jp|la|sg|我爱你|中国|公司|网络|集团))/', @$this->page->search->query, $msite)) {
            $site = json_decode(file_get_contents('http://opendata.baidu.com/api.php?resource_id=6735&oe=utf-8&query=site:'.@$msite[1]), 1);
            if (strlen(@$site[data][0][domain][0][append_text]) > 0) {
                return '
                <p class="white"><a class="pink" href="'.$this->page->urlTarget->getUrl().$this->page->urlTarget->link.'site:'.@$msite[1].'">索引量&nbsp;'.@$site[data][0][domain][0][append_text].'</a>&nbsp;更新&nbsp;'.date('Y-m-d H:i:s', @$site[data][0][_update_time])
                .'</p>';
            }
        }
    }

    public function getProduct($se)
    {
        $query = $this->page->search->query;
        $stk   = $this->page->config['cacheDir'];
        $url   = $this->page->urlTarget->getUrl();
//        if ($oas == 'on' || ($oas != 'on' && $osp != 'on' && $oec != 'on')) {
            // 搜索结果
            if (preg_match_all("/(?<=\" data\-tools=\'{\"title\":\")([^\"]+)(\",\"url\":\"http:)(\/\/www.baidu.com\/link\?url=[a-zA-Z0-9_\-]{43,640})(?=\"}'><a class=\"c-tip-icon\"><i class=\"c-icon c-icon-triangle-down-g\"><\/i><\/a><\/div>)/", @$se, $mserp));
            // 百度快照
            if (preg_match_all('/(?<=>&nbsp;-&nbsp;<a data-click="{\'rsv_snapshot\':\'1\'}" href="http:)(\/\/cache.baiducontent.com\/c\?m=[\da-f]+)(&p=[\da-f]+)(&newp=[\da-f]+)(&user=)(.+)(&p1=)(\d{1,3})(?=")/', @$se, $mcache));
            // 摘要
            if (preg_match_all('/(?<=<div class="c-abstract)( c-abstract-en)*(">)(.*)(?=<\/div><div class="f13">)/', @$se, $mabs)) {
                if (strlen(@$mabs[3][0]) > 0 && file_exists($stk.'abs-'.urlencode(@$query).'.txt') == false) {
                    file_put_contents($stk.'abs-'.urlencode($query).'.txt', str_replace('...', '', str_replace($this->page->search->ppRubish, $this->page->search->rpRubish, strip_tags(@$mabs[3][0]))), LOCK_EX);
                }
            }
            // 搜索结果页资源
            if (preg_match_all('/(?<=<div class="result c-container)( ?)(" id=")(\d{1,3})(" srcid=")(15\d{2})(" tpl=")(\w{2,28})(?=" ( ?)data-click="{)/', @$se, $msrcid)) {
                // F0
                if (preg_match_all("/(?<=F':)(\s?)(')([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})(?=',)/", $se, $mf)) {
                    // F1
                    if (preg_match_all("/(?<=F1':)(\s?)(')([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})(?=',)/", $se, $mf1)) {
                        // F2
                        if (preg_match_all("/(?<=F2':)(\s?)(')([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})(?=',)/", $se, $mf2)) {
                            // F3
                            if (preg_match_all("/(?<=F3':)(\s?)(')([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})([0-9A-F]{1})(?=',)/", $se, $mf3)) {
                                foreach ($msrcid[3] as $i => $v) {
                                    // 近似词
                                    if ($mf[4][$i] == 'F') {
                                        $homonym[$i] = '&nbsp;<span class="pinks" title="F0&nbsp;=&nbsp;xFxxxxxx&nbsp;多义词结果">多义词</span>';
                                    }
                                    elseif ($mf[4][$i] == '3') {
                                        $homonym[$i] = '&nbsp;<span class="pinks" title="F0&nbsp;=&nbsp;x3xxxxxx&nbsp;显示近似词搜索结果">近似词</span>';
                                    }
                                    // 时间限制
                                    elseif ($mf1[5][$i] == '6') {
                                        $lm[$i] = '&nbsp;<a itemprop="url" class="pinks" href="'.$url.'?s='.$query.'&amp;gpc=stf%3D'.(time() - 86400).'%2C'.time().'%7Cstftype%3D1" title="F1&nbsp;=&nbsp;xx6xxxxx&nbsp;0－24小时前更新快照&nbsp;yyyy年MM月dd日|hh小时前|mm分钟前|ss秒前" target="_blank" rel="nofollow noreferrer">1天内</a>';
                                    }
                                    elseif ($mf1[5][$i] == '5') {
                                        $lm[$i] = '&nbsp;<a itemprop="url" class="pinks" href="'.$url.'?s='.$query.'&amp;gpc=stf%3D'.(time() - 172800).'%2C'.(time() - 86400).'%7Cstftype%3D2" title="F1&nbsp;=&nbsp;xx5xxxxx&nbsp;24－48小时前更新快照&nbsp;yyyy年MM月dd日" target="_blank" rel="nofollow noreferrer">1－2天前</a>';
                                    }
                                    elseif ($mf1[5][$i] == '4') {
                                        $lm[$i] = '&nbsp;<a itemprop="url" class="pinks" href="'.$url.'?s='.$query.'&amp;gpc=stf%3D'.(time() - 604800).'%2C'.(time() - 172800).'%7Cstftype%3D2" title="F1&nbsp;=&nbsp;xx5xxxxx&nbsp;2－7天前前更新快照&nbsp;yyyy年MM月dd日" target="_blank" rel="nofollow noreferrer">2－7天前</a>';
                                    }
                                    // 百度知道|百度文库
                                    elseif ($mf1[3][$i] == 'B') {
                                        $iknow[$i] = '&nbsp;<span class="pinks" title="F1&nbsp;=&nbsp;xxxBxxxx&nbsp;百度文库">百度文库</span>';
                                    }
                                    elseif ($mf1[7][$i] == 'B') {
                                        $iknow[$i] = '&nbsp;<span class="pinks" title="F1&nbsp;=&nbsp;xxxBxxxx&nbsp;百度知道">百度知道</span>';
                                    }
                                    // 相关检索词
                                    elseif ($mf1[8][$i] == '5') {
                                        $sug9[$i] = '&nbsp;新热门内容';
                                    }
                                    elseif ($mf1[8][$i] == '3') {
                                        $sug9[$i] = '&nbsp;中热门内容';
                                    }
                                    elseif ($mf1[8][$i] == '0') {
                                        $sug9[$i] = '&nbsp;老热门内容';
                                    }
                                    // 标题类型
                                    elseif (@$mf2[9][$i].@$mf2[10][$i] == 'EB') {
                                        $title[$i] = '&nbsp;<span class="pinks" title="F2&nbsp;=&nbsp;xxxxxxEB&nbsp;锚文本&nbsp;-&nbsp;网页标题&nbsp;anchortext&nbsp;-&nbsp;title">锚文本&nbsp;-&nbsp;网页标题</span>';
                                    }
                                    elseif (@$mf2[9][$i].@$mf2[10][$i] == 'EA') {
                                        $title[$i] = '&nbsp;锚文本';
                                    }
                                    elseif (@$mf2[9][$i].@$mf2[10][$i] == '6F') {
                                        $title[$i] = '&nbsp;<a itemprop="url" class="pinks" href="/wordcount/#exp" title="F2&nbsp;=&nbsp;xxxxxx6F&nbsp;大字标题&nbsp;-&nbsp;网页标题&nbsp;headline&nbsp;-&nbsp;title" target="_blank" rel="external nofollow noreferrer">大字标题&nbsp;-&nbsp;网页标题</a>';
                                    }
                                    elseif (@$mf2[9][$i].@$mf2[10][$i] == '6E') {
                                        $title[$i] = '&nbsp;大字标题';
                                    }
                                    elseif (@$mf2[9][$i].@$mf2[10][$i] == '6A') {
                                        $title[$i] = '&nbsp;<span class="pinks" title="F2&nbsp;=&nbsp;xx2xxx6A&nbsp;标语&nbsp;slogan">标语</span>';
                                    }
                                    elseif (@$mf2[9][$i].@$mf2[10][$i] == '68') {
                                        $title[$i] = '&nbsp;<span class="pinks" title="F2&nbsp;=&nbsp;xxxxxx68&nbsp;网址&nbsp;url">网址</span>';
                                    }
                                    $nsrcid[$i] = array ($msrcid[5][$i], @$mserp[1][$i], $msrcid[3][$i], $mserp[3][$i], $msrcid[7][$i], @$title[$i], @$lm[$i], @$sug9[$i], @$homonym[$i], @$iknow[$i]);
                                    foreach ($mcache[7] as $key => $value) {
                                        if ($msrcid[3][$i] == $mcache[7][$key]) {
                                            $nsrcid[$i][10] = @$mcache[1][$key].@$mcache[3][$key];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
//        }
//        if ($osp == 'on' || ($oas != 'on' && $osp != 'on' && $oec != 'on')) {
            // fetch key
            if (preg_match_all('/(?<="  srcid=")(\d{1,5})("  fk=")([\d_]{0,6}?)([^_]{1,64})(" id=")(\d{1,2})(" tpl=")(\w{2,28})(" mu=")(.+)(?=" data-op="{\'y\':\'[a-zA-Z0-9]{8}\'}")/', @$se, $mfk)) {
                foreach ($mfk[6] as $i => $v) {
                    $nfk[$i] = array ($mfk[1][$i], $mfk[4][$i], $mfk[6][$i], $mfk[10][$i], $mfk[8][$i]);
                }
            }
//        }
        if (preg_match_all('/(?<="  srcid=")(\d{1,5})("  id=")(\d{1,2})(" tpl=")(\w{2,28})(" mu=")(.+)(?=" data-op="{\'y\':\'[a-zA-Z0-9]{8}\'}")/', @$se, $msp)) {
            foreach ($msp[3] as $i => $v) {
                $nsp[$i] = array ($msp[1][$i], '', $msp[3][$i], $msp[7][$i], $msp[5][$i]);
            }
        }
        // 三位一体
        if (strlen(@$nsrcid[0][0]) > 0) {
            if (strlen(@$nfk[0][0]) > 0) {
                if (strlen(@$nsp[0][0]) > 0) {
                    $n = array_merge($nsrcid, $nfk, $nsp);
                }
                else {
                    $n = array_merge($nsrcid, $nfk);
                }
            }
            elseif (strlen(@$nsp[0][0]) > 0) {
                $n = array_merge($nsrcid, $nsp);
            }
            else {
                $n = array_merge($nsrcid);
            }
        }
        elseif (strlen(@$nfk[0][0]) > 0) {
            if (strlen(@$nsp[0][0]) > 0) {
                $n = array_merge($nfk, $nsp);
            }
            else {
                $n = array_merge($nfk);
            }
        }
        elseif (strlen(@$nsp[0][0]) > 0) {
            $n = array_merge($nsp);
        }
        else{
            $n = '';
        }
        if ($n != '') {
            foreach ($n as $i => $v) {
                $i2[$i] = $v[2];
            }
            array_multisort($i2, $n);
        }

        $table = <<<html
 <div class="draglist">
        <table>
            <thead>
                <tr>
                    <th>标题</th>
                    <th>srcid</th>
                </tr>
            </thead>
            <tbody class="break">
html;
        $tbody = '';
        $queryn = $this->page->search->queryn;

        $srcid = include_once $this->page->config['require']['srcid']; // 已收集 1500 个资源号
        $np = include_once $this->page->config['require']['np'];
        $nr = include_once $this->page->config['require']['nr'];
        if(empty($n))
        {
            return '';
        }
        foreach ($n as $i => $v) {
//            echo $v[0].'--';
            $nn = array();
            foreach($srcid as $sv)
            {
                if ($v[0] == $sv[0])
                {
                    $nn = $sv;
                }
            }
            if (empty($nn)) continue;

            if (@$nn[4] == 'as') {
                $title = wordcount(stripslashes(htmlspecialchars_decode($n[$i][1], ENT_QUOTES)));
                $text  = str_replace($this->page->search->ppRubish, $this->page->search->rpRubish, stripslashes($n[$i][1]));
                $other = $n[$i][5].$n[$i][6].$n[$i][7].$n[$i][8].$n[$i][9];
                $tbody .= '<tr class="back-white">
                    <td>'.$n[$i][2].'&nbsp;<a itemprop="url" href="'.$n[$i][3].'" rel="external nofollow noreferrer" target="_blank"'.
                "title='{$title}'>{$text}</a>".$other;
                if (
                in_array(@$nn[0], [
                    1599,
                    1539,
                    1538,
                    1529,
                    1526,
                    1525,
                    1524,
                    1509
                ])
                ) {
                    $text = (str_replace($this->page->search->ppRubish, $this->page->search->rpRubish, strip_tags(@$mabs[3][@$jj + 0])));
                    $tbody.='<br><span class="tiny">'.$text.'</span>';
                    @$jj += 1;
                }
                $tbody .= '</td>';
                $tbody  .= '<td class="center" title="模板&nbsp;'.$n[$i][4].'">';
                        if (strlen(@$n[$i][10]) > 0) {
                            $tbody.= @$nn[1].'';
                        }
                        else
                            $tbody.= @$nn[1];
                        $tbody.= '</td></tr>';
                continue;
            }
            elseif (@$nn[4] == 'sp') {
                $tbody.='
                <tr class="back-egg">
                    <td>'.$n[$i][2].'&nbsp;<a itemprop="url" target="_blank" href="';
                if (@$nn[0] == 8041 ) {
                    $tbody.= '//sou.kuwo.cn/ws/NSearch?key='.$query;
                }
                elseif (@$nn[0] == 6006 ) {
                    $tbody.=  '//www.ip138.com/ips138.asp?ip='.$n[$i][1];
                }
                else {
                    $tbody.=  $n[$i][3];
                }
                $tbody.=  '" rel="external nofollow noreferrer" title="'.@$nn[4].'">'.$n[$i][1].'&nbsp;'.@$nn[1].'</a></td>
                    <td class="center">'.$n[$i][0].'</td>
                </tr>';
                continue;
            }
            elseif (@$nn[2] == 'sp') {
                    $tbody.= '
                <tr class="back-egg">
                    <td>'.$n[$i][2].'&nbsp;<a itemprop="url" target="_blank" href="'.preg_replace($np, $nr, $n[$i][3]).'" rel="external nofollow noreferrer" title="sp">'.@$nn[$i][1].'</a></td>
                    <td class="center" title="模板&nbsp;'.$n[$i][4].'">'.$n[$i][0].'</td>
                </tr>';
                continue;
            }
            elseif (@$nn[4] == 'ec') {
                    $tbody.= '
                <tr class="back-orange">
                    <td>'.$n[$i][2].'&nbsp;<a itemprop="url" target="_blank" href="'.preg_replace($np, $nr, $n[$i][3]).'" rel="external nofollow noreferrer" title="'.@$nn[$i][4].'">'.@$nn[$i][1].'</a></td>
                    <td class="center" title="模板&nbsp;'.$n[$i][4].'">'.$n[$i][0].'</td>
                </tr>';
                continue;
            }


            if (@$n[$i] != null) {
                $tbody .= '
                <tr class="back-azure">
                    <td>'.$n[$i][2].'&nbsp;未收进资源库</td>
                    <td class="center">'.$n[$i][0].'</td>
                </tr>';
            }

        }

        $table = !empty($tbody) ? $table.$tbody.<<<str
            </tbody>
        </table>
    </div>
str
            : '';

        return $table;

    }
}