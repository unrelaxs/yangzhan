<?php
class Url {
    protected $url;
    protected $uri;
    protected $http = 'http';
    protected $link;
    public function __construct(Page $page)
    {
        $url = '';
        $uri = '';
        if (basename(__FILE__) == 'index.php') {
            $uri = $page->config['dir'];
        }
        else {
            $uri = $_SERVER["PHP_SELF"];
        }
        if ($page->config['urlType'] == 1) {
            $url = '//'.$_SERVER['HTTP_HOST'].$uri;
        }
        else {
            $url = $uri;
        }
        if ($page->config['Https'])
        {
            $this->http = 'https';
        }
        if ($page->config['linkType'] == 1) {
            $l = ''; // 如果会修改服务器配置和写伪静态规则，在这里修改对应的伪静态网址
        }
        else {
            $l = '?s=';
        }
        $this->link= $l;
        $this->url = $url;
        $this->uri = $uri;
    }
    public function __get($name)
    {
        if(!property_exists($this, $name))
        {
            exit(__DIR__.':不存在属性:'.$name);
        }
        return $this->$name;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function getUri()
    {
        return $this->uri;
    }
}