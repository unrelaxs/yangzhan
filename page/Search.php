<?php
class Search {
    protected $ppRubish;   //pp垃圾
    protected $rpRubish;   //pp垃圾过滤的字符
    protected $keyword;
    protected $pn;          //分页显示数量

    protected $rpData;      //pp跟rp替换后的数据
    protected $querys;
    protected $query;
    protected $sugip;       //ip
    protected $sugl;        //查询到数据
    protected $queryn;

    public function __construct($keyword, $ppRubish, $rpRubish)
    {
        $this->keyword = $keyword;
        $this->ppRubish = $ppRubish;
        $this->rpRubish = $rpRubish;
        $this->pn       = $_GET['pn'] ?? 0;

    }

    public function __get($name)
    {
        if(!property_exists($this, $name))
        {
            exit(__DIR__.':不存在属性:'.$name);
        }
        return $this->$name;
    }

    public function search()
    {
        $p = array ('/(\s+)/', '/(http:\/\/)/', '/(&)/', '/(https:\/\/)/');
        $r = array ('%20', '', '%26', '');
        $s     = $this->keyword;
        $pp    = $this->ppRubish;
        $rp    = $this->rpRubish;
        $z = $this->rpData = str_replace($pp, $rp, preg_replace($p[1], $r[1], $s));
        $this->querys = str_replace($pp, $rp, htmlspecialchars(preg_replace($p, $r, $s)));
        $rrr =  array (' ', '');
        $this->queryn = htmlspecialchars(str_replace($pp, $rp, preg_replace($p, $rrr, $s)));
        $rr = array ('+', '', '%26');
        $this->query = htmlspecialchars(str_replace($pp, $rp, preg_replace($p, $rr, $s)));
        // 调用百度搜索框下拉模式 1 提示词
        $sugip = $this->sugip = array ('115.239.211.11', '115.239.211.12', '180.97.33.72', '180.97.33.73');
        shuffle ($sugip);
        $c = curl_init();
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_TIMEOUT, 3);
        curl_setopt($c, CURLOPT_URL, 'http://'.$sugip[0].'/su?action=opensearch&ie=UTF-8&wd='.$this->query);
        $this->sug1 = json_decode(curl_exec($c));
        curl_close($c);
        return $this->sugl;
    }
}