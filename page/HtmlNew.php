<?php
//html默认加载类名
return [
    'Canonical',
    'Description',
    'HeaderSearchForm',
    'Keyword',
    'Title',
    'TogetherSearch', //聚合搜索
    'FriendLink',
    'KeyWordSearchCount',
    'Paginate'
];