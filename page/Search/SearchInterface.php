<?php
interface SearchInterface {
    public function search();
    public function getCacheFile();
}