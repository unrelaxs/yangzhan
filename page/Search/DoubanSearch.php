<?php
class DoubanSearch implements SearchInterface{
    protected $api = 'https://movie.douban.com/j/search_subjects?type=movie&tag=%E6%9C%80%E6%96%B0&page_limit=500';
    protected $html;
    public function __construct(Html $html)
    {
        $this->html = $html;
    }
    public function getCacheFile()
    {
        return $this->html->getPage()->config['searchPlatform']['doubianFilm'];
    }
    public function search()
    {
        $word = [];
        $shldouban = $this->getCacheFile();
        // 百度搜索实时热点
        $c = curl_init();
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 8);
        curl_setopt($c, CURLOPT_TIMEOUT, 8);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($c, CURLOPT_URL, $this->api);
        $ikdouban = json_decode(curl_exec($c), 1);
        curl_close($c);
        if (isset($ikdouban['subjects'][0]['rate'])) {
            foreach ($ikdouban['subjects'] as $ikdouban1) {
                $word[] = strtolower(rtrim($ikdouban1['title']))."\n";
            }
            file_put_contents($shldouban, implode("\n", $word), LOCK_EX);
        }
        else {
            touch($shldouban);
        }
    }
}