<?php
class BaiduSearch implements SearchInterface{
    protected $api = 'http://entry.baidu.com/rp/api?di=api100002&qnum=40';
    protected $html;
    public function __construct(Html $html)
    {
        $this->html = $html;
    }
    public function getCacheFile()
    {
        return $this->html->getPage()->config['searchPlatform']['baiduHot'];
    }

    public function search()
    {
        $word = [];
        $shlbaidu = $this->getCacheFile();
        // 百度搜索实时热点
        $c = curl_init();
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 4);
        curl_setopt($c, CURLOPT_TIMEOUT, 4);
        curl_setopt($c, CURLOPT_URL, $this->api);
        $ikbaidu = json_decode(curl_exec($c), 1);
        curl_close($c);
        if ($ikbaidu['st'] > 0) {
            foreach ($ikbaidu['data']['recommends'] as $ikbaidu1) {
                if ($ikbaidu1['type'] == 109) {
                    $word[] = strtolower($ikbaidu1['word'])."\n";
                }
                else {
                    unlink($ikbaidu1);
                }
            }
            file_put_contents($shlbaidu, implode("\n", $word), LOCK_EX);
        }
        else {
            touch($shlbaidu);
        }

        return $word;
    }
}