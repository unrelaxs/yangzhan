<?php
class WangyiSearch implements SearchInterface{
    protected $api = 'http://music.163.com/api/playlist/detail?id=2884035';
    protected $html;
    public function __construct(Html $html)
    {
        $this->html = $html;
    }
    public function getCacheFile()
    {
        return $this->html->getPage()->config['searchPlatform']['wangyiMusic'];
    }

    public function search()
    {
        $word = [];
        $shl163 = $this->getCacheFile();
        // 百度搜索实时热点
        $c = curl_init();
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 4);
        curl_setopt($c, CURLOPT_TIMEOUT, 4);
        curl_setopt($c, CURLOPT_URL, $this->api);
        $ik163 = json_decode(curl_exec($c), 1);
        curl_close($c);
        if ($ik163['code'] == 200) {
            foreach ($ik163['result']['tracks'] as $ik1631) {
                $word[] = strtolower(rtrim($ik1631['name']));
            }
            file_put_contents($shl163, implode("\n", $word), LOCK_EX);
        }
        else {
            touch($shl163);
        }

        return $word;
    }
}