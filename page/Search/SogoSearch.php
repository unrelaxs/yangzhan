<?php
class SogoSearch implements SearchInterface{
    protected $api = 'http://changyan.sohu.com/api/3/topic/sogou_hotword';
    protected $html;
    public function __construct(Html $html)
    {
        $this->html = $html;
    }

    public function getCacheFile()
    {
        return $this->html->getPage()->config['searchPlatform']['sogouHot'];
    }

    public function search()
    {
        $word = [];
        $shlsogou = $this->getCacheFile();
        // 百度搜索实时热点
        $c = curl_init();
        curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 4);
        curl_setopt($c, CURLOPT_TIMEOUT, 4);
        curl_setopt($c, CURLOPT_URL, $this->api);
        $iksogou = json_decode(curl_exec($c), 1);
        curl_close($c);
        if (isset($iksogou['sogou_hot_words'][0])) {
            foreach ($iksogou['sogou_hot_words'] as $iksogou1) {
                $word[] = strtolower($iksogou1);
            }
            file_put_contents($shlsogou, implode("\n", $word), LOCK_EX);
        }
        else {
            touch($shlsogou);
        }

        return $word;
    }
}