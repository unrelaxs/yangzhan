<?php
class RnSearchKeyWord extends SearchKeyWordDecorate {
    protected $config;
    public function condition()
    {
        return $this->search->condition() && strlen($this->config['rn']) > 0;
    }
    public function getUrl()
    {
        return $this->search->getUrl().$this->config['crn'].$this->config['rn'];
    }
    public function setConfig($config)
    {
        $this->config = $config;
    }
}