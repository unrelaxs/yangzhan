<?php
class SearchKeyWord implements SearchKeyWordInterface{
    protected $config;
    public function __construct($config)
    {
        $this->config = $config;
    }

    //默认true
    public function condition()
    {
         return true;
    }
    public function getUrl()
    {
        return $this->config['url'].$this->config['query'];
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }
}