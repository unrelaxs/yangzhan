<?php
class GpcSearchKeyWord extends SearchKeyWordDecorate {
    protected $config;
    public function condition()
    {
        return $this->search->condition() && strlen($this->config['gpc']) > 0;
    }
    public function getUrl()
    {
        return $this->search->getUrl().$this->config['cgpc'].$this->config['gpc'];
    }
    public function setConfig($config)
    {
        $this->config = $config;
    }
}