<?php
class PnSearchKeyWord extends SearchKeyWordDecorate {
    protected $config;
    public function condition()
    {
        return $this->search->condition() && strlen($this->config['pn']) > 0;
    }
    public function getUrl()
    {
        return $this->search->getUrl().$this->config['cpn'].$this->config['pn'];
    }
    public function setConfig($config)
    {
        $this->config = $config;
    }
}