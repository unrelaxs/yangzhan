<?php
//百度关键字搜索
interface SearchKeyWordInterface {
    public function condition();
    public function getUrl();
    public function setConfig($config);
}