<?php
abstract class SearchKeyWordDecorate implements SearchKeyWordInterface{
    protected $search;
    public function __construct(SearchKeyWordInterface $search, $config)
    {
        $this->search = $search;
        $this->config = $config;
    }
}