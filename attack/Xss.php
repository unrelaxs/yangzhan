<?php
class Xss extends AbstractAttack implements Attack {
    public function check($data)
    {
        return parent::check($data);
    }
    public function pattern()
    {
        return
            "[\\'\\\"\\;\\*\\<\\>].*\\bon[a-zA-Z]{3,15}[\\s\\r\\n\\v\\f]*\\=|\\b(?:expression)\\(|\\<script[\\s\\\\\\/]|\\<\\!\\[cdata\\[|\\b(?:eval|alert|prompt|msgbox)\\s*\\(|url\\((?:\\#|data|javascript)"
            ;
    }
}