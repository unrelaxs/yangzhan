<?php
abstract class AbstractAttack {
    private $checkStatus = true;
    //正则
    abstract function pattern();

    public function check($arr){
        $pattern = $this->pattern();
        $this->checkData($arr, $pattern);
        return $this->checkStatus;
    }

    public function checkData($arr, $pattern)
    {
        foreach($arr as $key => $value) {
            if (!is_array($key)) {
                $this->checkPattern($key, $pattern);
            }
            else {
                $this->check($key, $pattern);
            }
            if (!is_array($value)) {
                $this->checkPattern($value, $pattern);}
            else {
                $this->check($value, $pattern);
            }
        }
    }


    function checkPattern($str, $v) {
        if (is_array($v))
        {
            foreach($v as $key => $value) {
                if (preg_match("/".$value."/is", $str) == 1 || preg_match("/".$value."/is", urlencode($str)) == 1) {
                    $this->checkStatus = false;
                    return false;
                }
            }
        } else {
            if (preg_match("/".$v."/is", $str) == 1 || preg_match("/".$v."/is", urlencode($str)) == 1) {
                $this->checkStatus = false;
                return false;
            }
        }

    }


}