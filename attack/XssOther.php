<?php
class XssOther extends AbstractAttack implements Attack {
    public function check($data)
    {
        return parent::check($data);
    }
    public function pattern()
    {
        return
            "\\=\\+\\/v(?:8|9|\\+|\\/)|\\%0acontent\\-(?:id|location|type|transfer\\-encoding)"
            ;
    }
}