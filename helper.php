<?php
function html($data)
{
    $str = <<<html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="cmn-Hans" xml:lang="cmn-Hans">
<head><meta charset="UTF-8">
html;

    if (isset($data['title']))
    {
        $str.= <<<html
        <title>{$data['title']}</title>
html;

    }
}

//字数统计函数
function wordcount($str, $encoding = 'UTF-8') {
    if(strtolower($encoding) == 'gbk') {
        $encoding = 'gb18030';
    }
    if(!is_string($str) || $str === '')
        return false;
    $subLen = 0;
    for ($i = 0; $i < iconv_strlen($str, $encoding); $i++) {
        strlen(iconv_substr($str, $i, 1, $encoding)) == 1 ? $subLen += 1 : $subLen += 2;
    }
    return $subLen;
}