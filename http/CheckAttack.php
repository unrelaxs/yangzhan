<?php
class CheckAttack {
    public function check() {
        $referer = empty($_SERVER['HTTP_REFERER']) ? array() : array($_SERVER['HTTP_REFERER']);
        $query_string = empty($_SERVER["QUERY_STRING"]) ? array() : array($_SERVER["QUERY_STRING"]);

        $attackArr = [
            new Other(),
            new Sql(),
            new Xss(),
        ];
        $otherAttachArr = [
            new XssOther()
        ];
        $res = [];
        foreach ($attackArr as $attack)
        {
            $res[] = $attack->check($_GET);
            $res[] = $attack->check($_POST);
            $res[] = $attack->check($_COOKIE);
            $res[] = $attack->check($referer);
        }

        foreach ($otherAttachArr as $attack)
        {
            $res[] = $attack->check($query_string);
        }
        if (in_array(false, $res))
        {
            return false;
        }
        return true;
    }
}