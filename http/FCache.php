<?php
class FCache {
    public function __construct($cache_path = 'stock/', $cache_time = 86400, $cache_extension = '.stk') {
        $this->cache_path = $cache_path;
        $this->cache_time = $cache_time;
        $this->cache_extension = $cache_extension;
        if (!file_exists($this->cache_path)) {
            mkdir($this->cache_path, 0777);
        }
    }
    public function add($key, $value) {
        $filename = $this->_get_cache_file($key);
        file_put_contents($filename, serialize($value), LOCK_EX);
    }
    public function delete($key) {
        $filename = $this->_get_cache_file($key);
        unlink($filename);
    }
    public function get($key) {
        if ($this->_has_cache($key)) {
            $filename = $this->_get_cache_file($key);
            $value = file_get_contents($filename);
            if (empty($value)) {
                return false;
            }
            return unserialize($value);
        }
    }
    public function flush() {
        $fp = opendir($this->cache_path);
        while(!false == ($fn = readdir($fp))) {
            if($fn == '.' || $fn == '..') {
                continue;
            }
            unlink($this->cache_path . $fn);
        }
    }
    private function _has_cache($key) {
        $filename = $this->_get_cache_file($key);
        if(file_exists($filename) && (filemtime($filename) + $this->cache_time >= time())) {
            return true;
        }
        return false;
    }
    private function _is_valid_key($key) {
        if ($key != null) {
            return true;
        }
        return false;
    }
    private function _safe_filename($key) {
        if ($this->_is_valid_key($key)) {
            return md5($key);
        }
        return 'unvalid_cache_key';
    }
    private function _get_cache_file($key) {
        return $this->cache_path . $this->_safe_filename($key) . $this->cache_extension;
    }
}