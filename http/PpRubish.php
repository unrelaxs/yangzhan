<?php
class PpRubish {
    protected $pp;
    protected $rp;
    public function __construct($file)
    {
        $data = file_get_contents($file);
        $data = explode("\r\n", $data);
        foreach ($data as $value)
        {
            if (!empty($value))
            {
                $temp = explode('<=>', $value);
                $this->pp[] = $temp[0];
                $this->rp[] = $temp[1];
            }
        }
    }
    public function getData()
    {
        return [
            'pp'    => $this->pp,
            'rp'    => $this->rp
        ];
    }
}